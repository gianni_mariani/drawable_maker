#!/usr/bin/env python

_LICENCE="""
    This software is Copyright (c) 2016 Everbility Pty Ltd.
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import os
import os.path
import sys
import re
import textwrap
import errno


FILENAME_PATTERN = re.compile('(.)(/drawable-([^/]+)/)(.)')
PREFIX=1
RESOLUTION=3
FILENAME=4

# Map of resolutions
RESOLUTIONS = {
   'xxxhdpi' : 4.0,
   'xxhdpi' : 3.0,
   'xhdpi' : 2.0,
   'hdpi' : 1.5,
   'mdpi' : 1.0,
 }

SORTED_RESOLUTIONS = RESOLUTIONS.items()
SORTED_RESOLUTIONS.sort(key=lambda tup: tup[1])

EXAMPLE_USAGE="""

    Example usage:
        Converts images to sizex x sizey and adds them to drawables in destination
        
    {0} --sizex 36 --sizey 36 --dest src/main/res icon1.svg icon2.svg
    
""".format(sys.argv[0])

DESCRIPTION="""\
Convert image to png and place converted images in android compatible drawable directories.

Supported resolutions are: {0}
""".format(", ".join(i for i, j in SORTED_RESOLUTIONS))

ImageMagick_CMD = 'convert'

def convert_command(**kargs):
    fmt = '{cmd} -resize {resize} -background none {source} {dest}'
    return fmt.format(cmd=ImageMagick_CMD, **kargs)

def convert_with_color_tint_command(**kargs):
    return """\
    {cmd} -resize {resize} -background none {source} -alpha off -fill '{color}' \
        -colorize {percent}% -alpha on {dest}
    """.format(cmd=ImageMagick_CMD, **kargs)

def execute(command):
    if os.system(command) != 0:
        raise Exception('Failed to execute command "%s"' % command)

def process_image(filename, destination, destname, sizex, sizey, color, percent):
    resize = '{0}x{1}'.format(sizex, sizey)
    resultfilename = destination + '/' + destname + '.png'
    if color:
        convertCommand = convert_with_color_tint_command(
                resize=resize, source=filename, color=color, percent=percent, dest=resultfilename)
    else:
        convertCommand = 'convert -resize %s -background none %s %s' % (resize, filename, resultfilename)

    compressCommand = 'pngquant --force --ext .png ' + resultfilename
    
    execute(convertCommand)
    execute(compressCommand)


def create_path_if_not_existing(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise
        
def drawable_placer(filename, destination, destname, sizex, sizey, color, percent):
    for name, multiplier in RESOLUTIONS.items(): 
        dest = destination + 'drawable-%s' % name
        #dest = destination + '/drawable-%s' % name
        create_path_if_not_existing(dest)
        xmult = sizex * multiplier
        ymult = sizey * multiplier
        process_image(filename, dest, destname, xmult, ymult, color, percent)
        

def main(argv):
    
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=DESCRIPTION,
            epilog=textwrap.dedent(EXAMPLE_USAGE))
    parser.add_argument('sourcefiles', metavar='file', type=str, nargs='*',
                   help='Source images to convert.')
    parser.add_argument('--dest', type=str, help='Destination path.(Contains drawable directories)', required=True)
    parser.add_argument('--destname', type=str, help='Destination file name', required=True)
    parser.add_argument('--sizex', type=float, help='x resolution in dp.', required=True)
    parser.add_argument('--sizey', type=float, help='y resolution in dp.', required=True)
    parser.add_argument('--color', type=str, help='Resulting color.')
    parser.add_argument('--percent', type=int, help='Percent of colorize.')
    
    args = parser.parse_args()
    
    for file in args.sourcefiles:
        drawable_placer(file, args.dest, args.destname, args.sizex, args.sizey, args.color, args.percent)
        

if __name__ == "__main__":
    exit(main(sys.argv))
